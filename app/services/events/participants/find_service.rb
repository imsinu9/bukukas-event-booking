module Events
  module Participants
    class FindService
      attr_reader :participants

      def initialize(event)
        @event = event
      end

      def find
        @participants = @event.participants
      end
    end
  end
end
