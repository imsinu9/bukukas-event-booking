module Bookings
  class PriceCalculator
    attr_reader :booking_amount, :discount

    def initialize(event, user)
      @event = event
      @user = user
    end

    def calculate
      @booking_amount = if discount_applicable?
        price_with_gender_discount
      else
        @event.price
      end
    end

    private

    def discount_applicable?
      @user.gender.female?
    end

    def price_with_gender_discount
      @discount = @event.price * 0.05
      (@event.price - @discount).ceil
    end
  end
end
