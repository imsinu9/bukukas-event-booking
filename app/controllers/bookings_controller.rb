class BookingsController < ApplicationController
  before_action :set_event
  before_action :initialize_booking, only: [:new]

  def new;end

  def create
    booking_service = Bookings::EventBookingForm.new(@event, current_user, booking_params)
    booking_service.save

    redirect_to event_path(@event)
  end

  def cancel
    cancellation_service = Bookings::CancellationForm.new(@event, current_user)
    cancellation_service.save

    redirect_to event_path(@event)
  end

  private

  def set_event
    @event = Event.find(params[:id])
  end

  def initialize_booking
    @booking_calulator = Bookings::PriceCalculator.new(@event, current_user)
    @booking = @event.bookings.new(amount: @booking_calulator.calculate)
  end

  def booking_params
    params.require(:booking).permit(:amount)
  end
end
