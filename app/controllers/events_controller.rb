class EventsController < ApplicationController
  before_action :set_events, only: [:index]
  before_action :set_event, only: [:show]

  def index;end

  def show
    @participants = Events::Participants::FindService.new(@event).find
    @participants.includes(:user)
  end

  private

  def set_events
    @events = Event.newest
  end

  def set_event
    @event = Event.find params[:id]
  end
end
