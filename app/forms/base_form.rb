class BaseForm
  include ActiveModel::Model
  include ActiveModel::Validations
  include ActiveModel::Validations::Callbacks

  define_model_callbacks :save, only: [ :before ]

  def save
    run_callbacks(:save) do
      super
    end
  end
end
