module Bookings
  class CancellationForm < BaseForm
    attr_reader :user_participation, :event

    validates :event, presence: true
    validate :cancellable?

    def initialize(event, user, opts={})
      @event = event
      @user = user
    end

    def save
      if valid?
        @user_participation.booking.cancel!
        @user_participation.cancel!
      end
    end

    private

    def cancellable?
      @user_participation = @user.participations.for_event(@event.id)&.first
      @user_participation.present?
    end
  end
end
