module Bookings
  class EventBookingForm < BaseForm
    attr_reader :booking, :event, :user
    attr_accessor :participant

    validates :event, presence: true
    validates :user, presence: true
    validate :bookable?

    before_save :create_participant

    def initialize(event, user, params)
      @event = event
      @user = user
      @params = params
    end

    def save
      if valid?
        ActiveRecord::Base.transaction do
          run_callbacks(:save) do
            @booking = @participant.create_booking(
              @params.merge(
                event_id: @event.id,
                status: :confirmed
              )
            )
          end
        end
      end
    end

    private

    def bookable?
      !event.past_event?
    end

    def create_participant
      @participant = Participant.create!(user_id: @user.id, event_id: @event.id)
    end
  end
end
