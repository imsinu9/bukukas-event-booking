class User < ApplicationRecord
  extend Enumerize

  enumerize :gender, in: [:male, :female]
  devise :database_authenticatable, :registerable, :validatable

  has_many :participations, class_name: 'Actor'

  validates :name, presence: true
  validates :email, presence: true
end
