class Participant < Actor
  acts_as_paranoid

  has_one :booking, foreign_key: :actor_id

  def cancel!
    self.destroy
  end
end
