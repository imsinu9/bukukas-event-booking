class Booking < ApplicationRecord
  extend Enumerize

  enumerize :status, in: [:new, :confirmed, :cancelled], default: :new, scope: true
  belongs_to :event
  belongs_to :participant, foreign_key: :actor_id

  validates :status, presence: true

  def cancel!
    update!(status: :cancelled)
  end
end
