class Payment < ApplicationRecord
  extend Enumerize

  enumerize :status, in: [:created, :paid], default: :created
  belongs_to :booking

  validates :amount, presence: true
  validates :status, presence: true
end
