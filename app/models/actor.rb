class Actor < ApplicationRecord
  belongs_to :user
  belongs_to :event

  scope :for_event, -> (event_id) { where(event_id: event_id) }
end
