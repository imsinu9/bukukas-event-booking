class Event < ApplicationRecord
  validates :title, presence: true
  validates :price, presence: true
  validates :event_start_time, presence: true
  validates :event_end_time, presence: true

  has_many :bookings
  has_many :participants, through: :bookings

  scope :newest, -> { order(:created_at) }

  def past_event?
    Time.now >= event_end_time
  end
end
