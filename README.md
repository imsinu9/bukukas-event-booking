# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

  ```
    2.5.1
  ```

* System dependencies

  ```
  Rails: 5.2
  ```

* Assumptions

** Booking

1. Individual booking.
2. No Ticketing Logic

** Event

1. No variants.
2. Fixed pricing.

* Database Setup
  ```
  rails db:create && rails db:migrate && rails db:seed
  ```

* How to run the test suite