Rails.application.routes.draw do
  devise_for :users do
    get 'sign_in', to: 'devise/sessions#new'
  end

  root to: 'events#index'

  resources :events, only: [:index, :show] do
    member do
      resources :bookings, only: [:new, :create] do
        collection do
          post :cancel
        end
      end
    end
  end
end
