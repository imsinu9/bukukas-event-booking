User.create!(name: 'User 1', email: "user1@example.com", password: '123456', password_confirmation: '123456', gender: 'male')
User.create!(name: 'User 2', email: "user2@example.com", password: '123456', password_confirmation: '123456', gender: 'male')
User.create!(name: 'User 3', email: "user3@example.com", password: '123456', password_confirmation: '123456', gender: 'female')
User.create!(name: 'User 4', email: "user4@example.com", password: '123456', password_confirmation: '123456', gender: 'female')

Event.create!(title: 'E001', price: 1000, event_start_time: Time.now, event_end_time: Time.now + 4.hours, created_at: Time.now - 1.days)
Event.create!(title: 'E002', price: 1500, event_start_time: Time.now - 2.days, event_end_time: Time.now - 1.days)
Event.create!(title: 'E003', price: 2000, event_start_time: Time.now + 1.day, event_end_time: Time.now + 2.days)