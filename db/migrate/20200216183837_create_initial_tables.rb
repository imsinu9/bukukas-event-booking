class CreateInitialTables < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.string :title, null: false
      t.float :price, null: false, default: 0
      t.datetime :event_start_time, null: false
      t.datetime :event_end_time, null: false
      t.timestamps
    end

    create_table :users do |t|
      t.string :name, null: false, default: ''
      t.string :email, null: false, default: ''
      t.string :gender
      t.string :encrypted_password, null: false, default: ''


      t.timestamps
    end
    add_index :users, :email, unique: true

    create_table :bookings do |t|
      t.integer :actor_id, null: false
      t.integer :event_id, null: false
      t.float :amount, null: false
      t.string :status, null: false

      t.timestamps
    end

    create_table :actors do |t|
      t.integer :user_id, null: false
      t.integer :event_id, null: false
      t.string :type, null: false
      t.datetime :deleted_at

      t.timestamps
    end
  end
end
